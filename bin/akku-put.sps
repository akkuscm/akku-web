#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2018 G. Weinholt
;; SPDX-License-Identifier: GPL-3.0-or-later

;; Akku.scm CGI script.

(import
  (rnrs (6))
  (only (chezscheme) getenv))

(define (string-index str c start end)
  (let lp ((i start))
    (cond ((= i end)
           #f)
          ((char=? (string-ref str i) c)
           i)
          (else
           (lp (+ i 1))))))

(define string-split
  (case-lambda
    ((str c max start end)
     (cond ((zero? max)
            (list (substring str start end)))
           ((string-index str c start end) =>
            (lambda (i)
              (cons (substring str start i)
                    (string-split str c (- max 1) (+ i 1) end))))
           (else
            (list (substring str start end)))))
    ((str c max start)
     (string-split str c max start (string-length str)))
    ((str c max)
     (string-split str c max 0 (string-length str)))
    ((str c)
     (string-split str c -1 0 (string-length str)))))

(define (path-filename path)
  (let ((parts (string-split path #\/)))
    (cond ((> (length parts) 1)
           (let ((fn (car (reverse parts))))
             (cond ((member fn '("." "..")) #f)
                   (else fn))))
          (else #f))))

(define (send-response status message)
  ;; 201: Created; 202: Accepted; 406: Not acceptable
  (display "Status: ")
  (display status)
  (newline)
  (display "Content-Type: text/plain; charset=utf-8\n")
  (newline)
  (when message
    (display message)
    (newline)))

(let* ((from-client (standard-input-port))
       (payload (get-bytevector-n from-client 100000)))
  (cond
    ((eof-object? payload)
     (send-response 406 "No data"))
    ((not (port-eof? from-client))
     (send-response 406 "Too much data"))
    ((path-filename (getenv "REQUEST_URI"))
     => (lambda (fn)
          (call-with-port (open-file-output-port (string-append "/home/tmp/" fn)
                                                 (file-options no-fail)
                                                 (buffer-mode block))
            (lambda (p)
              (put-bytevector p payload)))
          (send-response 202 (string-append "Queued " fn))))
    (else
     (send-response 400 "Bad URI"))))
