#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018, 2019, 2020 G. Weinholt
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;; Static Gopher site generation for akkuscm.org

(import
  (rnrs (6))
  (only (srfi :1 lists) last take append-map)
  (only (srfi :13 strings) string-index string-prefix? string-trim-right)
  (only (srfi :67 compare-procedures) <? string-compare-ci)
  (only (akku private compat) open-process-ports putenv)
  (akku lib manifest)
  (only (akku lib utils) path-join application-data-directory
        url-join mkdir/recursive sanitized-name)
  (chibi match)
  (wak fmt)
  (semver versions)
  (semver ranges)
  (xitomatl alists))

(define (open-process cmd)
  (let-values (((to-stdin from-stdout from-stderr _process-id)
                (open-process-ports cmd (buffer-mode block)
                                    (native-transcoder))))
    (close-port to-stdin)
    (close-port from-stderr)
    from-stdout))

(define (take-non-duplicate-packages ts* n)
  (let lp ((n n) (pkgname #f) (pkg* '()) (ts* ts*))
    (cond ((= n 0) (reverse pkg*))
          (else
           (let ((pkg (car ts*)))
             (match pkg
               [(_date pkgname^ . _)
                (if (equal? pkgname^ pkgname)
                    (lp n pkgname pkg* (cdr ts*))
                    (lp (- n 1) pkgname^ (cons pkg pkg*) (cdr ts*)))]))))))

(define (write-map filename entries)
  (display filename)
  (newline)
  (when (file-exists? filename)
    (delete-file filename))
  (call-with-output-file filename
    (lambda (p)
      (for-each (lambda (entry)
                  (cond ((string? entry)
                         (put-string p entry)
                         (newline p))
                        ((vector? entry)
                         (display (vector-ref entry 0) p)
                         (let lp ((i 1))
                           (unless (fx=? i (vector-length entry))
                             (display #\tab p)
                             (display (vector-ref entry i) p)
                             (lp (+ i 1))))
                         (newline p))
                        ((eqv? entry #f))
                        (else
                         (write (list 'TODO entry))
                         (newline)
                         #F)))
                entries))))

;; Many thanks to John Cowan for figlet
(define (figlet string)
  (putenv "STR" string)
  (get-string-all (open-process "figlet \"$STR\"")))

(define figlet-akku (string-trim-right (figlet "Akku")))

(define (html-link title url)
  (vector (string-append "h" title)
          (string-append "URL:" url)))
(define (html-link* url title)
  (html-link title url))
(define (menu-link title selector)
  (vector (string-append "1" title)
          selector))

(define (template title nav main)
  `(,(string-append "!" title)
    ,@(if nav nav '())
    ,figlet-akku
    ;; TODO: search
    ,@main))

(define (string->info-lines x)
  (let ((inp (open-string-input-port x)))
    (let lp ((ret '()))
      (if (port-eof? inp)
          (reverse ret)
          (lp (cons (string-append "i" (get-line inp) "\t")
                    ret))))))

(define (make-site-index)
  (template
   "Akku.scm - Scheme package manager"
   #f
   `(,@(heading "Package management made easy")
     "Akku.scm is a language package manager for Scheme. It grabs hold"
     "of code and vigorously shakes it until it behaves properly."
     ""
     " - One command to install everything needed for your project."
     " - Separately declare your dependencies and locked versions."
     " - Automatically convert R7RS libraries for use in R6RS projects."
     " - Audited build scripts for use with FFI libraries."
     " - Mirror of R7RS libraries from Snow."
     ""
     ,(html-link "Download from GitLab" "https://gitlab.com/akkuscm/akku/-/releases")
     ,(menu-link "Package index" "/packages")
     ;; Demo

     ;; Cards
     ,@(heading "Features")
     " - Dependency solver"
     " - Cryptographic signing"
     " - Command line interface"
     " - Locked dependencies"
     " - Project-based work flow"

     ,@(heading "Supported standards")

     ;; http://www.r6rs.org/ http://www.r7rs.org/
     " - R6RS and R7RS Scheme"
     ;; https://semver.org/
     " - Semantic Versioning 2.0.0"
     ;; https://spdx.org/
     " - SPDX license expressions"
     ;; https://www.openpgp.org/
     " - OpenPGP signatures"

     ,@(heading "License")
     "Akku.scm is distributed under the GNU General Public License
version 3.0 or later."

     ,@(heading "Supported Schemes")
     ,(html-link* "https://www.scheme.com/" "Chez Scheme [*]")
     ,(html-link* "http://synthcode.com/wiki/chibi-scheme" "Chibi Scheme [**]")
     ,(html-link* "https://www.gnu.org/software/guile/" "GNU Guile [*]")
     ,(html-link* "http://practical-scheme.net/gauche/" "Gauche Scheme [**]")
     ,(html-link* "http://ikarus-scheme.org/" "Ikarus Scheme")
     ,(html-link* "https://github.com/leppie/IronScheme" "IronScheme")
     ,(html-link* "http://larcenists.org/" "Larceny Scheme")
     ,(html-link* "https://scheme.fail/" "Loko Scheme")
     ,(html-link* "http://mosh.monaos.org/files/doc/text/About-txt.html" "Mosh Scheme")
     ,(html-link* "https://racket-lang.org/" "Racket (plt-r6rs)")
     ,(html-link* "https://bitbucket.org/ktakashi/sagittarius-scheme/wiki/Home"
                  "Sagittarius Scheme")
     ,(html-link* "http://marcomaggi.github.io/vicare.html" "Vicare Scheme")
     ,(html-link* "http://www.littlewingpinball.net/mediawiki/index.php/Ypsilon"
                  "Ypsilon Scheme")
     "[*] Can run Akku  [**] R7RS only"

     ,@(heading "Tested platforms")
     " - Cygwin"
     " - FreeBSD"
     " - GNU/Linux"
     " - MSYS2"
     " - OpenBSD"
     " - macOS"

     ,@(heading "Latest uploads")
     ,@(map (match-lambda
             [(date name version . _)
              (menu-link (fmt #f (name->string name)
                              (space-to 15) " "
                              version
                              (space-to 40) " [" date "]")
                         (package-url name))])
            (take-non-duplicate-packages (call-with-input-file "timestamps.scm" read)
                                         13))

     ,@(heading "External links")
     ;; Contact details
     ,(html-link "Wiki" "https://gitlab.com/akkuscm/akku/wikis/home")
     ;; ircs://chat.freenode.net/akku
     ,(html-link "#akku @ Freenode" "https://webchat.freenode.net/?channels=akku")
     ,(html-link "Blog" "https://weinholt.se/tag/akku/1/")
     ,(html-link "Scheme Discord" "https://discord.gg/ZcTYrdx")
     ,(html-link "Site source code" "https://gitlab.com/akkuscm/akku-web")

     ,@(heading "Virtual hosts on this server")
     "%"

     ,@(heading "Internal links")
     "*")))

(define (make-privacy-page)
  (template
   "Akku.scm - Privacy policy"
   #f
   `(""
     "This page describes how the Akku client and web site handle
personally identifiable information."
     ,@(heading "Command line client")
     "The akku command line client does not collect personally identifiable
information. Some subcommands of the client make network requests on
behalf of the user and the target of the request may in turn use the
data sent over the network:

 - The commands that make these requests are marked with a \"*\"
   in the akku help output.

 - Network requests are only made to the extent that they are necessary
   to carry out the requested operations, e.g. to download an updated
   package index, download a package, to publish a package, etc."
     ,@(heading "Web and Gopher sites")
     "The akkuscm.org and related web sites handle personally identifiable
information in the following manner:

 - Web server requests are saved in standard log files.

 - The web server logs are rotated weekly and are shredded after four
   rotations.

 - The web server logs are used purely for anonymous statistics
   and troubleshooting.

 - The web site displays the names of authors of software.

Questions and requests related to Akku and privacy should be sent to
privacy at this server's domain name."
     "*")))

(define (read-package-index index-filename) ;XXX: should be in akku
  (let ((packages (make-hashtable equal-hash equal?)))
    ;; Read packages from the index.
    (call-with-input-file index-filename
      (lambda (p)
        (let lp ()
          (match (read p)
            ((? eof-object?) #f)
            (('package ('name name)
                       ('versions version* ...))
             (hashtable-set! packages name
                             (make-package name (map parse-version version*)))
             (lp))
            (else (lp))))))             ;allow for future expansion
    packages))

(define (package-url pkg)
  (let ((pkg (if (package? pkg) (package-name pkg) pkg)))
    (url-join "/packages" (sanitized-name pkg))))

(define (name->string x)
  (if (string? x)
      x
      (call-with-string-output-port
        (lambda (p) (display x p)))))

(define (quote-package-name x)
  (if (string? x)
      x
      (call-with-string-output-port
        (lambda (p)
          (display #\" p)
          (display x p)
          (display #\" p)))))

(define (compare-names x y)
  (define (strip-paren x)
    (if (string? x)
        x
        (let ((str
               (call-with-string-output-port
                 (lambda (p)
                   (display x p)))))
          (substring str 1 (- (string-length str) 1)))))
  (string-compare-ci (strip-paren x) (strip-paren y)))

(define (make-package-index idx)
  (let ((names (hashtable-keys idx)))
    (vector-sort! (lambda (x y) (<? compare-names x y)) names)
    (template
     "Akku.scm packages"
     #f
     `(""
       ,(menu-link "Back" "/")
       ""
       ,@(map
          (lambda (name)
            (let* ((pkg (hashtable-ref idx name #f))
                   (ver (last (package-version* pkg))))
              (menu-link (fmt #f (name->string name)
                              (space-to 25) " "
                              (ellipses "..."
                                        (pad 12 (trim 11 (version-number ver)))
                                        (trim 30 (cond ((version-synopsis ver) => car)
                                                       (else "-")))))
                         (package-url pkg))))
          (vector->list names))))))

(define (heading str)
  `("" ,@ (string->info-lines str)
    ,(string-append "i" (make-string (min 70 (string-length str)) #\=)
                    "\t") ""))

(define (render-packages idx)
  (define (lock->entries lock)
    (match (assq-ref lock 'location)
      [(('git remote-url))
       `(,(html-link (fmt #f "Git repository: " remote-url)
                     remote-url)
         ,(fmt #f "Git revision: " (car (assq-ref lock 'revision)))
         ,(cond ((assq-ref lock 'tag #f) =>
                 (lambda (tag)
                   (fmt #f "Tag: " (car tag))))
                (else #f)))]

      [(('url url))
       `(,(fmt #f "Source URL: " url))]))
  (define (pkg-html prev pkg next)
    (define ver (last (package-version* pkg)))
    (define (show-deps title dep*)
      (if (null? dep*)
          '()
          `(,@(heading title)
            ,@(map (match-lambda
                    [(dep-name dep-range)
                     (menu-link (fmt #f (cat dep-name " " (space-to 15)
                                             dep-range " "
                                             (space-to 40)
                                             "("
                                             (semver-range->string
                                              (semver-range-desugar
                                               (string->semver-range dep-range)))
                                             ")"))
                                (package-url dep-name))])
                   dep*))))
    (template
     (string-append "Package: " (name->string (package-name pkg)))
     `(,(if prev (menu-link "<< Previous" prev) #f)
       ,(menu-link "Package list" "/packages")
       ,(if next (menu-link "Next >>" next) #f))
     `(,@(heading (fmt #f (name->string (package-name pkg)) " - "
                       (version-number ver)))
       ,@(string->info-lines
          (fmt #f
               (with-width 75
                           (wrap-lines
                            (cond ((version-synopsis ver) => car)
                                  (else "(no synopsis)"))))))
       ""
       ,@(if (version-description ver)
             (string->info-lines
              (fmt #f
                   (fmt-join (lambda (paragraph)
                               (if (string-prefix? " " paragraph)
                                   (cat paragraph nl)
                                   (with-width 75 (wrap-lines paragraph))))
                             (version-description ver))))
             '())

       ""
       "$ akku update"
       ,(fmt #f "$ akku install " (quote-package-name (package-name pkg)))
       "$ .akku/env"

       ,@(heading "Authors")
       ,@(string->info-lines
          (fmt #f
               (fmt-join (lambda (author)
                           (cond ((string-index author #\<) =>
                                  (lambda (i)
                                    (cat (substring author 0 i) nl)))
                                 (else (cat author nl))))
                         (version-authors ver))))

       ,@(if (version-homepage ver)
             `(,@(heading "Homepage")
               ,(html-link (car (version-homepage ver))
                           (car (version-homepage ver))))
             '())

       ,@(heading "License")
       ,(fmt #f (car (version-license ver)))
       ,@(show-deps "Dependencies" (version-depends ver))
       ,@(show-deps "Dependencies (development)" (version-depends/dev ver))
       ,@(show-deps "Conflicts" (version-conflicts ver))
       ,@(heading "Source code")
       ,@(lock->entries (version-lock ver)))))
  (define (render-package prev pkg next)
    (let* ((dir (path-join "builg/packages" (sanitized-name (package-name pkg))))
           (fn (path-join dir "gophermap")))
      (mkdir/recursive dir)
      (write-map fn
                 (pkg-html (and prev (package-url prev))
                           pkg
                           (and next (package-url next))))))
  (let-values (((_ pkgs) (hashtable-entries idx)))
    (vector-sort! (lambda (x y)
                    (<? compare-names (package-name x) (package-name y)))
                  pkgs)
    (do ((i 0 (+ i 1)))
        ((= i (vector-length pkgs)))
      (render-package (and (> i 0) (vector-ref pkgs (- i 1)))
                      (vector-ref pkgs i)
                      (and (< i (- (vector-length pkgs) 1))
                           (vector-ref pkgs (+ i 1)))))))

(define (render-manpage filename manpage)
  (putenv "MANPAGE" manpage)
  (let ((manual (open-process "groff -man -Tutf8 \"$MANPAGE\"|col -bx")))
    (call-with-output-file filename
    (lambda (p)
      (put-string p (get-string-all manual))))))

(mkdir/recursive "builg")
(write-map "builg/gophermap" (make-site-index))

(mkdir/recursive "builg/privacy")
(write-map "builg/privacy/gophermap" (make-privacy-page))

(mkdir/recursive "builg/packages")

(let ((index (read-package-index "Akku-origin.scm")))
  (write-map "builg/packages/gophermap" (make-package-index index))
  (render-packages index))

(render-manpage "builg/akku.1.txt"
                ".akku/src/akku/docs/akku.1") ;a bit presuming
