#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright © 2018, 2019, 2020, 2021, 2023 Gwen Weinholt
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
#!r6rs

;;; Static web site generation for akkuscm.org

(import
  (rnrs (6))
  (only (srfi :1 lists) last take delete-duplicates iota append-map)
  (only (srfi :13 strings) string-index string-trim-both
        string-prefix? string-join)
  (only (srfi :67 compare-procedures) <? string-compare-ci)
  (only (akku private compat) open-process-ports putenv)
  (akku lib manifest)
  (only (akku lib utils) path-join application-data-directory
        url-join mkdir/recursive sanitized-name)
  (akku lib schemedb)
  (chibi match)
  (industria openpgp)
  (semver versions)
  (semver ranges)
  (wak htmlprag)
  (wak fmt)
  (xitomatl alists)
  (xitomatl lists))

(define-record-type pkgmeta
  (sealed #t)
  (fields name version lock license artifacts))

(define-record-type pkg+lib
  (sealed #t)
  (fields pkg pkgname pkgver libtype libname impl))

(define metadata-file "metadata.scm")

(define implementations
  '((chezscheme . "Chez Scheme")
    (chibi . "Chibi-Scheme")
    (chicken . "CHICKEN")
    (cyclone . "Cyclone Scheme")
    (digamma . "Digamma")
    (foment . "Foment")
    (gauce . "Gauche")
    (guile . "GNU Guile")
    (ikarus . "Ikarus")
    (ironscheme . "IronScheme")
    (kawa . "Kawa")
    (larceny . "Larceny")
    (loko . "Loko Scheme")
    (mosh . "Mosh")
    (mzscheme . "Racket")
    (nmosh . "Mosh")
    (rapid-scheme . "Rapid Scheme")
    (sagittarius . "Sagittarius")
    (vicare . "Vicare")
    (ypsilon . "Ypsilon")))

(define (open-process cmd)
  (let-values (((to-stdin from-stdout from-stderr _process-id)
                (open-process-ports cmd (buffer-mode block)
                                    (native-transcoder))))
    (close-port to-stdin)
    (close-port from-stderr)
    from-stdout))

(define (take-non-duplicate-packages ts* n)
  (let lp ((n n) (pkgname #f) (pkg* '()) (ts* ts*))
    (cond ((= n 0) (reverse pkg*))
          (else
           (let ((pkg (car ts*)))
             (match pkg
               [(_date pkgname^ . _)
                (if (equal? pkgname^ pkgname)
                    (lp n pkgname pkg* (cdr ts*))
                    (lp (- n 1) pkgname^ (cons pkg pkg*) (cdr ts*)))]))))))

(define (split-every-n list n)
  (assert (> n 0))
  (let lp ((list list) (k n) (acc '()) (ret '()))
    (cond ((null? list)
           (if (null? acc)
               (reverse ret)
               (reverse (cons (reverse acc) ret))))
          ((eqv? k 0)
           (lp list n '() (cons (reverse acc) ret)))
          (else
           (lp (cdr list) (- k 1) (cons (car list) acc) ret)))))

(define (string-take-find s c)
  (cond ((string-index s c) =>
         (lambda (i) (substring s 0 i)))
        (else s)))

(define (remove-comment author)
  (string-take-find author #\())

(define (remove-email author)
  (string-take-find author #\<))

(define (extract-name author)
  (string-trim-both (remove-comment (remove-email author))))

(define (write-shtml filename shtml)
  (display filename)
  (newline)
  (when (file-exists? filename)
    (delete-file filename))
  (call-with-output-file filename
    (lambda (p)
      (write-shtml-as-html shtml p))))

(define (template title nav main)
  `(*TOP*
    (*DECL* doctype html)
    (html (^ (lang "en"))
          (head (meta (^ (charset "utf-8")))
                (meta (^ (name "viewport") (content "width=device-width, initial-scale=1, shrink-to-fit=no")))
                (meta (^ (name "description") (content "Akku.scm - Scheme package manager")))
                (link (^ (rel "icon") (type "image/png") (href "/assets/img/favicon.png")))
                ;; (link (^ (rel "icon") (type "image/svg+xml") (href "/assets/img/favicon.svg")))
                (title ,title)
                ;; Bootstrap 4.1.1
                (link (^ (rel "stylesheet") (href "/assets/css/bootstrap.min.css")))
                (link (^ (href "/assets/css/akkuscm.css") (rel "stylesheet"))))
          (body
           (div (^ (class "d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow"))
                (h5 (^ (class "my-0 mr-md-auto font-weight-normal"))
                    (a (^ (class "navbar-brand") (href "/"))
                       (img (^ (alt "Akku.scm")
                               (src "/assets/img/akku-horizontal-30px.png")
                               (height "30")))))
                (form (^ (method "get") (id "search") (action "https://duckduckgo.com/"))
                      (input (^ (type "hidden") (name "sites") (value "akkuscm.org")))
                      (input (^ (class "search") (type "text") (name "q") (maxlength "300")
                                (placeholder "DuckDuckGo site search")))
                      (imput (^ (type "submit") (value "Search")
                                (style "visibility: hidden;"))))
                (nav (^ (class "my-2 my-md-0 mr-md-3"))
                     (a (^ (class "p-2 text-dark") (href "https://discord.gg/hC5nF8aNbe"))
                        "Discord")
                     " "
                     (a (^ (class "p-2 text-dark") (href "https://gitlab.com/akkuscm/akku/wikis/home"))
                        "Wiki")
                     (a (^ (class "p-2 text-dark") (href "/docs/manpage.html"))
                        "Documentation")
                     " "
                     (a (^ (class "p-2 text-dark") (href "/identifiers/"))
                        "Identifiers")
                     " "
                     (a (^ (class "p-2 text-dark") (href "/libraries/"))
                        "Libraries")
                     " "
                     (a (^ (class "p-2 text-dark") (href "/packages/"))
                        "Packages")
                     ))
           ,@(if nav `((nav ,nav)) '())
           (main (^ (role "main") (class "container"))
                 ,@main)
           (footer
            (^ (class "page-footer"))
            ;; Contact details
            (small "© 2024 Gwen Weinholt" " · "
                   (a (^ (href "mailto:info@akkuscm.org"))
                      "Email")
                   " · "
                   (a (^ (href "ircs://irc.libera.chat:6697/akku"))
                      "Chat")
                   " · "
                   (a (^ (href "https://weinholt.se/tag/akku/1/"))
                      "Blog")
                   " · "
                   (a (^ (href "https://discord.gg/ZcTYrdx"))
                      "Scheme Discord")
                   " · "
                   (a (^ (href "gopher://akkuscm.org:70/1/;akkuscm.org"))
                      "Gopher")
                   " · "
                   (a (^ (href "/privacy/"))
                      "Privacy")
                   " · "
                   (a (^ (href "https://gitlab.com/akkuscm/akku-web"))
                      "Source code")))))))

(define (make-site-index timestamps)
  (template
   "Akku.scm - Scheme package manager"
   #f
   `((h1 "Package management made easy")
     (p (^ (class "lead"))
        "Akku.scm is a language package manager for Scheme. It
grabs hold of code and shakes it vigorously until it behaves properly."
        (ul (li "One command to install everything needed for your project.")
            (li "Separately declare your dependencies and locked versions.")
            (li "Automatically convert R" (sup "7") "RS libraries for use in R" (sup "6") "RS projects.")
            (li "Audited build scripts for use with FFI libraries.")
            (li "Mirror of R" (sup "7") "RS libraries from "
                (a (^ (href "http://snow-fort.org")) "Snow")
                ".")))
     (p
      (a (^ (class "btn btn-success") (role "button")
            (href "https://gitlab.com/akkuscm/akku/-/releases"))
         ;; https://lists.gnu.org/archive/html/guix-patches/2020-08/msg00531.html
         ;; AUR
         "Download from GitLab"))
     ;; Demo
     (p (^ (class "text-center"))
        (script (^ (src "https://asciinema.org/a/259410.js")
                   (id "asciicast-259410")
                   (async "async"))))
     ;; Cards
     (p
      (div (^ (class "card-deck"))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Features")
                     (p (^ (class "card-text"))
                        (ul (li "Dependency solver")
                            (li "Cryptographic signing")
                            (li "Command line interface")
                            (li "Locked dependencies")
                            (li "Project-based work flow")))))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Supported standards")
                     (p (^ (class "card-text"))
                        (ul (^ (class "list"))
                            (li (a (^ (href "http://www.r6rs.org/")) "R" (sup "6") "RS")
                                " and "
                                (a (^ (href "http://www.r7rs.org/")) "R" (sup "7") "RS Scheme"))
                            (li (a (^ (href "https://semver.org/")) "Semantic Versioning 2.0.0"))
                            (li (a (^ (href "https://spdx.org/")) "SPDX license expressions"))
                            (li (a (^ (href "https://www.openpgp.org/")) "OpenPGP signatures"))))))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "License")
                     (p (^ (class "card-text"))
                        "Akku.scm is distributed under the GNU General Public License version 3.0 or later."
                        (br)
                        (a (^ (href "http://www.gnu.org/licenses/gpl.html"))
                           (img (^ (src "/assets/img/gplv3-127x51.png")
                                   (alt "GPL-3.0-or-later")))))))))
     (p
      (div (^ (class "card-deck"))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Supported Schemes")
                     (p (^ (class "card-text"))
                        (ul (^ (class "list"))
                            (li (a (^ (href "https://www.scheme.com/")) "Chez Scheme") (sup "✻"))
                            (li (a (^ (href "http://synthcode.com/wiki/chibi-scheme")) "Chibi-Scheme") (sup "‡"))
                            (li (a (^ (href "https://www.gnu.org/software/guile/")) "GNU Guile") (sup "✻"))
                            (li (a (^ (href "http://practical-scheme.net/gauche/")) "Gauche Scheme") (sup "‡"))
                            (li (a (^ (href "http://ikarus-scheme.org/")) "Ikarus Scheme"))
                            (li (a (^ (href "https://github.com/leppie/IronScheme")) "IronScheme"))
                            (li (a (^ (href "http://larcenists.org/")) "Larceny Scheme"))
                            (li (a (^ (href "https://scheme.fail/")) "Loko Scheme"))
                            (li (a (^ (href "http://mosh.monaos.org/files/doc/text/About-txt.html")) "Mosh Scheme"))
                            (li (a (^ (href "https://racket-lang.org/")) "Racket (plt-r6rs)"))
                            (li (a (^ (href "https://bitbucket.org/ktakashi/sagittarius-scheme/wiki/Home")) "Sagittarius Scheme"))
                            (li (a (^ (href "http://marcomaggi.github.io/vicare.html")) "Vicare Scheme"))
                            (li (a (^ (href "http://www.littlewingpinball.net/mediawiki/index.php/Ypsilon")) "Ypsilon Scheme")))
                        (small "(✻) Can run Akku  (‡) R" (sup "7") "RS only"))))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Tested platforms")
                     (p (^ (class "card-text"))
                        (ul (^ (class "list"))
                            (li "Cygwin")
                            (li "FreeBSD")
                            (li "GNU/Linux")
                            (li "MSYS2")
                            (li "OpenBSD")
                            (li "macOS")
                            ))))
           (div (^ (class "card"))
                (div (^ (class "card-body"))
                     (h5 (^ (class "card-title")) "Latest uploads")
                     (p (^ (class "card-text"))
                        (ul (^ (class "list"))
                            ,@(map (match-lambda
                                    [(date name version issuers)
                                     `(li (a (^ (href ,(package-url name))
                                                (title ,(name->string name) " " ,version))
                                             ,(name->string name))
                                          (small " [" ,date "]"))])
                                   (take-non-duplicate-packages timestamps 13)))))))))))

(define (make-privacy-page)
  (template
   "Akku.scm - Privacy policy"
   #f
   `((h1 "Privacy policy")
     (p "This page describes how the Akku client and web site handle personally
identifiable information.")

     (h2 "Command line client")
     (p "The akku command line client does not collect personally
identifiable information. Some subcommands of the client make network
requests on behalf of the user and the target of the request may in turn
use the data sent over the network:"
        (ul (li "The commands that make these requests are marked with a \"*\" in the
akku help output.")
            (li "Network requests are only made to the extent that
they are necessary to carry out the requested operations, e.g. to
download an updated package index, download a package, to publish a
package, etc.")))

     (h2 "Web site")
     (p "The akkuscm.org and related web sites handle personally identifiable information
in the following manner:"
        (ul (li "Web server requests are saved in standard log files.")
            (li "The web server logs are rotated weekly and are shredded after four rotations.")
            (li "The web server logs are used purely for anonymous
statistics and troubleshooting.")
            (li "The web site displays the names of authors of software.")))
     (p "Questions and requests related to Akku and privacy should be sent to "
        (i "privacy at this domain name") "."))))

(define (read-package-index index-filename) ;XXX: should be in akku
  (let ((packages (make-hashtable equal-hash equal?)))
    ;; Read packages from the index.
    (call-with-input-file index-filename
      (lambda (p)
        (let lp ()
          (match (read p)
            ((? eof-object?) #f)
            (('package ('name name)
                       ('versions version* ...))
             (hashtable-set! packages name
                             (make-package name (map parse-version version*)))
             (lp))
            (else (lp))))))             ;allow for future expansion
    packages))

(define (package-url pkg)
  (let ((pkg (if (package? pkg) (package-name pkg) pkg)))
    (url-join (url-join "/packages" (sanitized-name pkg)) "")))

(define (name->string x)
  (if (string? x)
      x
      (call-with-string-output-port
        (lambda (p) (display x p)))))

(define (quote-package-name x)
  (if (string? x)
      x
      (call-with-string-output-port
        (lambda (p)
          (display #\" p)
          (display x p)
          (display #\" p)))))

(define (compare-names x y)
  (define (strip-paren x)
    (if (string? x)
        x
        (let ((str
               (call-with-string-output-port
                 (lambda (p)
                   (display x p)))))
          (substring str 1 (- (string-length str) 1)))))
  (string-compare-ci (strip-paren x) (strip-paren y)))

(define (make-package-index idx)
  (let ((names (hashtable-keys idx)))
    (vector-sort! (lambda (x y) (<? compare-names x y)) names)
    (template
     "Akku.scm packages"
     #f
     `((table
        (^ (class "table table-hover table-striped"))
        (thead
         (tr (th (^ (scope "col")) "Name")
             (th (^ (scope "col")) "Latest version")
             (th (^ (scope "col")) "Synopsis")))
        (tbody
         ,@(map (lambda (name)
                  (let* ((pkg (hashtable-ref idx name #f))
                         (ver (last (package-version* pkg))))
                    `(tr (td (a (^ (href ,(package-url pkg))) ,(name->string name)))
                         (td ,(version-number ver))
                         (td ,@(or (version-synopsis ver)
                                   '((i "synopsis missing")))))))
                (vector->list names))))))))

(define (render-packages idx timestamps keyring signature-issuers metadata)
  (define package-timestamps            ;name => (date version)
    (let ((ht (make-hashtable equal-hash equal?)))
      (for-each (match-lambda
                 [(date name version . _)
                  (hashtable-update! ht name
                                     (lambda (old) (cons (list date version) old))
                                     '())])
                timestamps)
      ht))
  (define (lock-html lock)
    (match (assq-ref lock 'location)
      [(('git remote-url))
       `((a (^ (href ,remote-url)) ,remote-url) " "
         (span (^ (class "badge badge-pill badge-primary")) "git")
         (br)
         (small ,(car (assq-ref lock 'revision))) (br)
         ,(cond ((assq-ref lock 'tag #f) =>
                 (lambda (tag) `(("Tag: " ,(car tag)))))
                (else '())))]
      [(('url url))
       `((a (^ (href ,url)) ,url) " "
         (span (^ (class "badge badge-pill badge-primary")) "url")
         (br)
         #;
         (small ,(car (assq-ref lock 'revision))) (br)
         )]))
  (define (pkg-html prev pkg next)
    (define ver (last (package-version* pkg)))
    (define meta
      (cond ((hashtable-ref metadata (cons (package-name pkg) (version-number ver))
                            #f))
            (else
             (error 'pkg-html "Missing package in metadata.scm"
                    (package-name pkg) (version-number ver)))))
    (define first-signer
      (cond ((hashtable-ref signature-issuers
                            (cons (package-name pkg) (version-number ver))
                            #f) => car)
            (else
             (error 'pkg-html "Missing package signature in timestamps.scm"))))
    (define signer-key (cond ((hashtable-ref keyring first-signer #f))
                             (else (error 'pkg-html "Missing OpenPGP key in Akku-keyring.pgp"
                                          (number->string first-signer 16)))))
    (define signer
      (extract-name
       (cond ((find openpgp-user-id? signer-key) => openpgp-user-id-value)
             (else (error 'pkg-html "Invalid key without user id"
                          (number->string first-signer 16))))))
    (template
     (string-append "Akku.scm: " (name->string (package-name pkg)))
     `(ul (^ (class "nav justify-content-center"))
          (li (^ (class "nav-item"))
              (a ,(if prev
                      `(^ (class "nav-link") (href ,prev))
                      '(^ (class "nav-link disabled") (href "#")))
                 "« Previous"))
          (li (^ (class "nav-item pull-right"))
              (a ,(if next
                      `(^ (class "nav-link") (href ,next))
                      '(^ (class "nav-link disabled") (href "#")))
                 "Next »")))
     `((h1 ,(name->string (package-name pkg)) " "
           (span (^ (class "badge badge-pill badge-primary"))
                 ,(version-number ver)))
       (h3 ,@(or (version-synopsis ver) '((i "synopsis missing"))))
       ,@(map (lambda (paragraph)
                (if (and (> (string-length paragraph) 0)
                         (char=? #\space (string-ref paragraph 0)))
                    `(pre ,paragraph)
                    `(p ,paragraph)))
              (or (version-description ver) '()))

       (pre (samp "$ akku update\n"
                  "$ akku install " ,(quote-package-name (package-name pkg)) "\n"
                  "$ .akku/env"))

       (div (^ (class "card-deck my-4"))
            (div (^ (class "card"))
                 (div (^ (class "card-body"))
                      (h5 (^ (class "card-title")) "Authors")
                      (p (^ (class "card-text"))
                         ,@(map (lambda (author) `(p ,(extract-name author)))
                                (version-authors ver)))
                      ,(if (and (not (list? (package-name pkg)))
                                (or (not (eqv? (length (version-authors ver)) 1))
                                    (and (pair? (version-authors ver))
                                         (not (string-ci=? (extract-name (car (version-authors ver)))
                                                           signer)))))
                           `(p (^ (class "card-text text-muted"))
                               "Uploaded to Akku by " ,signer)
                           "")))
            (div (^ (class "card"))
                 (div (^ (class "card-body"))
                      (h5 (^ (class "card-title")) "Homepage")
                      (p (^ (class "card-text"))
                         ,(if (version-homepage ver)
                              `(a (^ (href ,(version-homepage ver))
                                     (target "_blank"))
                                  ,(version-homepage ver))
                              "Information missing."))))

            (div (^ (class "card"))
                 (div (^ (class "card-body"))
                      (h5 (^ (class "card-title")) "License")
                      (p (^ (class "card-text"))
                         ,(version-license ver)))))

       (div (^ (class "card-deck my-4"))
            (div (^ (class "card"))
                 (div (^ (class "card-body"))
                      (h5 (^ (class "card-title")) "Dependencies")
                      (p (^ (class "card-text"))
                         ;; FIXME: render dev dependencies
                         ,@(if (pair? (version-depends ver))
                               (map (match-lambda
                                     [(name range)
                                      `(div (^ (class "row"))
                                            (div (^ (class "col"))
                                                 (a (^ (href
                                                        ,(package-url
                                                          (hashtable-ref idx name #f))))
                                                    ,(name->string name)))
                                            (div (^ (class "col"))
                                                 (span
                                                  (^ (title ,(semver-range->string
                                                              (semver-range-desugar
                                                               (string->semver-range range)))))
                                                  ,range)))])
                                    (version-depends ver))
                               '("No dependencies.")))))

            (div (^ (class "card"))
                 (div (^ (class "card-body"))
                      (h5 (^ (class "card-title")) "Source code")
                      (p (^ (class "card-text"))
                         ,@(lock-html (version-lock ver))))))

       (div (^ (class "card-deck my-4"))
            (div (^ (class "card"))
                 (div (^ (class "card-body"))
                      (h5 (^ (class "card-title")) "Package contents")
                      (p (^ (class "card-text"))
                         (ul (^ (class "list-group list-group-flush"))
                             ,@(map
                                (lambda (artifact)
                                  (define (lookup-impl field*)
                                    ;; Get the implementation field or
                                    ;; #f. Tells us which
                                    ;; implementation this artifact is
                                    ;; written for.
                                    (cond ((assq-ref field* 'implementation #f) =>
                                           (lambda (impl) (car impl)))
                                          (else #f)))
                                  (define (impl-pill field*)
                                    (cond ((lookup-impl field*) =>
                                           (lambda (impl)
                                             `(" "
                                               (span (^ (class "badge badge-pill badge-success"))
                                                     ,(or (assq-ref implementations impl #f)
                                                          (name->string impl))))))
                                          (else '())))
                                  (define (exports field*)
                                    (cond ((assq-ref field* 'exports #f) =>
                                           (lambda (id*)
                                             `((li (^ (class "list-group-item"))
                                                    "→ "
                                                    ,(string-join (map symbol->string id*)
                                                                  " ")))))
                                          (else '())))
                                  (define (imports field*)
                                    (define (linkify lib)
                                      (define (srfi-number x)
                                        (cond ((symbol? x)
                                               (let ((str (symbol->string x)))
                                                 (cond
                                                   ((and (or (string-prefix? ":" str)
                                                             (string-prefix? "s" str))
                                                         (string->number
                                                          (substring str 1 (string-length str)))))
                                                   ((and (string-prefix? "srfi-" str)
                                                         (string->number
                                                          (substring str 5 (string-length str)))))
                                                   (else #f))))
                                              ((number? x) x)
                                              (else #f)))
                                      (match lib
                                        [('srfi n . _)
                                         (cond ((srfi-number n) =>
                                                (lambda (n)
                                                  `((a (^ (href ,(string-append
                                                                  "https://srfi.schemers.org/srfi-"
                                                                  (number->string n) "/")))
                                                       ,(name->string lib))
                                                    " "
                                                    (span (^ (class "badge badge-pill badge-info"))
                                                          "SRFI"))))
                                               (else
                                                (list (name->string lib))))]
                                        [else (list (name->string lib))]))
                                    (define (maybe-builtin lib impl)
                                      (cond ((r6rs-builtin-library? lib #f)
                                             `(" " (span (^ (class "badge badge-pill badge-primary"))
                                                         "R6")))
                                             ((r7rs-builtin-library? lib #f)
                                              `(" " (span (^ (class "badge badge-pill badge-primary"))
                                                          "R7")))
                                             ((or (r7rs-builtin-library? lib impl)
                                                  (r7rs-builtin-library? lib impl))
                                              `(" " (span (^ (class "badge badge-pill badge-success"))
                                                          "Built-in")))
                                             (else
                                              '())))
                                    (cond ((assq-ref field* 'imports #f) =>
                                           (lambda (imp*)
                                             (define impl (lookup-impl field*))
                                             (map (match-lambda
                                                   [(lib _ver)
                                                    `(li (^ (class "list-group-item"))
                                                         "← "
                                                         ,@(linkify lib)
                                                         ,@(maybe-builtin lib impl))])
                                                  (delete-duplicates imp*))))
                                          (else '())))
                                  (define (usage-pill field*)
                                    (cond ((assq-ref field* 'usage #f) =>
                                           (lambda (usage*)
                                             (map (match-lambda
                                                   ['internal
                                                    '(" "
                                                      (span (^ (class "badge badge-pill badge-warning"))
                                                            "Internal"))]
                                                   ['test
                                                    '(" "
                                                      (span (^ (class "badge badge-pill badge-secondary"))
                                                            "Test"))]
                                                   [x '(span)])
                                                  (car usage*))))
                                          (else '())))

                                  (match artifact
                                    [('r6rs-library ('name name) . field*)
                                     `(li (^ (class "list-group-item"))
                                          (strong ,(name->string name))
                                          " "
                                          (span (^ (class "badge badge-pill badge-primary"))
                                                "R6")
                                          ,@(impl-pill field*)
                                          ,@(usage-pill field*)
                                          (ul (^ (class "list-group"))
                                              ,@(exports field*)
                                              ,@(imports field*)))]
                                    [('r7rs-library ('name name) . field*)
                                     `(li (^ (class "list-group-item"))
                                          (strong ,(name->string name))
                                          " "
                                          (span (^ (class "badge badge-pill badge-primary"))
                                                "R7")
                                          ,@(impl-pill field*)
                                          ,@(usage-pill field*)
                                          (ul (^ (class "list-group"))
                                              ,@(exports field*)
                                              ,@(imports field*)))]
                                    [('module #f . _)
                                     ""]
                                    [('module name . field*)
                                     `(li (^ (class "list-group-item"))
                                          (strong ,(name->string name))
                                          " "
                                          (span (^ (class "badge badge-pill badge-primary"))
                                                "Module")
                                          ,@(impl-pill field*)
                                          ,@(usage-pill field*)
                                          (ul (^ (class "list-group"))
                                              ,@(exports field*)
                                              ,@(imports field*)))]
                                    [x
                                     (display "TODO: ")
                                     (write x)
                                     (newline)
                                     ""]))

                                (pkgmeta-artifacts meta))))))

            (div (^ (class "card"))
                 (div (^ (class "card-body"))
                      (h5 (^ (class "card-title")) "Version history")
                      (p (^ (class "card-text"))
                         (ul (^ (class "list-group list-group-flush"))
                             ,@(map (match-lambda
                                     [(date version)
                                      `(li (^ (class "list-group-item"))
                                           ,date " ~ " ,version)])
                                    (reverse
                                     (hashtable-ref package-timestamps (package-name pkg) '())))))))))))
  (define (render-package prev pkg next)
    (let* ((dir (path-join "build/packages" (sanitized-name (package-name pkg))))
           (fn (path-join dir "index.html")))
      (mkdir/recursive dir)
      (write-shtml fn
                   (pkg-html (and prev (package-url prev))
                             pkg
                             (and next (package-url next))))))
  (let-values (((_ pkgs) (hashtable-entries idx)))
    (vector-sort! (lambda (x y)
                    (<? compare-names (package-name x) (package-name y)))
                  pkgs)
    (do ((i 0 (+ i 1)))
        ((= i (vector-length pkgs)))
      (render-package (and (> i 0) (vector-ref pkgs (- i 1)))
                      (vector-ref pkgs i)
                      (and (< i (- (vector-length pkgs) 1))
                           (vector-ref pkgs (+ i 1)))))))

(define (gather-libraries+identifiers idx metadata)
  (define identifiers (make-eq-hashtable)) ;id => (pkg+lib ...)
  (define libraries (make-hashtable equal-hash equal?))   ;lib-name => (pkg+lib ...)
  (let-values ([(_ metavec) (hashtable-entries metadata)])
    (vector-for-each
     (lambda (meta)                     ;XXX: package versions are ordered
       (define (internal-usage? field*)
         (cond ((assq-ref field* 'usage #f) =>
                (lambda (usage*) (memq 'internal (car usage*))))
               (else #f)))

       (define (handle-exports library-type library-name field*)
         (let ((pkg+lib (make-pkg+lib (hashtable-ref idx (pkgmeta-name meta) #f)
                                      (pkgmeta-name meta)
                                      (pkgmeta-version meta)
                                      library-type library-name
                                      (cond ((assq-ref field* 'implementation #f) => car)
                                            (else #f)))))
           (hashtable-update! libraries library-name
                              (lambda (old) (cons pkg+lib old))
                              '())
           (unless (internal-usage? field*)
             (cond ((assq-ref field* 'exports #f) =>
                    (lambda (export*)
                      (for-each
                       (lambda (id)
                         (unless (eqv? 0 (string-length (symbol->string id)))
                           (hashtable-update! identifiers id
                                              (lambda (old) (cons pkg+lib old))
                                              '())))
                       export*)))))))

       ;; Only use the identifiers from the latest version of each
       ;; package. There's probably little point in showing
       ;; identifiers that were removed.
       (when (equal? (pkgmeta-version meta)
                     (version-number
                      (last (package-version*
                             (hashtable-ref idx (pkgmeta-name meta) #f)))))
         (for-each
          (match-lambda
           [('r6rs-library ('name name) . field*)
            (handle-exports 'r6rs-library name field*)]
           [('r7rs-library ('name name) . field*)
            (handle-exports 'r7rs-library name field*)]
           [('module #f . _) #f]
           [('module name . field*)
            (handle-exports 'module name field*)]
           [else #f])
          (pkgmeta-artifacts meta))))
     metavec))

  (values libraries identifiers))

(define (make-identifier-index-pages identifiers)
  (let* ([ids (vector-sort (lambda (x y)
                             (string<? (symbol->string x)
                                       (symbol->string y)))
                           (hashtable-keys identifiers))]
         [split-ids (split-every-n (vector->list ids) 500)]
         [n-pages (length split-ids)])
    (map
     (lambda (ids page-number)
       (define pagination
         `(ul (^ (class "nav justify-content-center"))
              ,(map (lambda (n)
                      `(li (^ (class "nav-item"))
                           ,(if (= n page-number)
                                `(a (^ (class "nav-link disabled") (href "#"))
                                    ,(number->string n))
                                `(a (^ (class "nav-link") (href ,(fmt #f n ".html")))
                                    ,(number->string n)))))
                    (iota n-pages 1))))

       (template
        "Akku.scm identifier index"
        #f
        `(,pagination
          (table
           (^ (class "table table-hover"))
           (thead
            (tr (th (^ (scope "col")) "Identifier")
                (th (^ (scope "col")) "Library")
                (th (^ (scope "col")) "Package")))
           "\n" (*COMMENT* "The source for this table is metadata.scm at archive.akkuscm.org") "\n"
           (tbody
            (^ (class "table-group-divider"))
            ,@(append-map
               (lambda (id k)
                 (let ((pkg+lib* (hashtable-ref identifiers id '())))
                   (map (lambda (pkg+lib n)
                          `(tr ,@(if (odd? k) `((^ (class "workaround-striped"))) '())
                               (td (^ (class "cup-runneth-over"))
                                   ,(if (eqv? n 0) (symbol->string id) ""))
                               (td (^ (style "width: 40%"))
                                   ,(fmt #f (pkg+lib-libname pkg+lib))
                                   " "
                                   ,(case (pkg+lib-libtype pkg+lib)
                                      ((r6rs-library)
                                       '(span (^ (class "badge badge-pill badge-primary"))
                                              "R6"))
                                      ((r7rs-library)
                                       '(span (^ (class "badge badge-pill badge-primary"))
                                              "R7"))
                                      ((module)
                                       '(span (^ (class "badge badge-pill badge-primary"))
                                              "Module"))
                                      (else "")))
                               (td (^ (style "width: 25%"))
                                   (a (^ (href ,(package-url (pkg+lib-pkg pkg+lib))))
                                      ,(name->string (pkg+lib-pkgname pkg+lib))))))
                        (list-sort (lambda (x y)
                                     (if (equal? (pkg+lib-libname x) (pkg+lib-libname y))
                                         (<? compare-names (pkg+lib-pkgname x) (pkg+lib-pkgname y))
                                         (<? compare-names (pkg+lib-libname x) (pkg+lib-libname y))))
                                   pkg+lib*)
                        (iota (length pkg+lib*)))))
               ids
               (iota (length ids)))))
          ,pagination)))
     split-ids
     (iota n-pages 1))))

(define (make-library-index libraries)
  (let ([libs (vector-sort (lambda (x y)
                             (<? compare-names x y))
                           (hashtable-keys libraries))])
    (template
     "Akku.scm library index"
     #f
     `((table
        (^ (class "table table-hover table-striped"))
        (thead
         (tr (th (^ (scope "col")) "Library")
             (th (^ (scope "col")) "Packages")))
        "\n" (*COMMENT* "The source for this table is metadata.scm at archive.akkuscm.org") "\n"
        (tbody
         (^ (class "table-group-divider"))
         ,@(map
            (lambda (lib-name)
              (let ((pkg+lib* (hashtable-ref libraries lib-name '())))
                (let ((types (delete-duplicates
                              (list-sort (lambda (x y)
                                           (string<? (symbol->string x)
                                                     (symbol->string y)))
                                         (map pkg+lib-libtype pkg+lib*)))))
                  `(tr (td (span ,(name->string lib-name))
                           ,@(append-map
                              (lambda (type)
                                (case type
                                  ((r6rs-library)
                                   '(" " (span (^ (class "badge badge-pill badge-primary"))
                                               "R6")))
                                  ((r7rs-library)
                                   '(" " (span (^ (class "badge badge-pill badge-primary"))
                                               "R7")))
                                  ((module)
                                   '(" " (span (^ (class "badge badge-pill badge-primary"))
                                               "Module")))
                                  (else '())))
                              types))

                       (td ,@(intersperse
                              (map (lambda (pkg+lib)
                                     `(a (^ (href ,(package-url (pkg+lib-pkg pkg+lib))))
                                         ,(name->string (pkg+lib-pkgname pkg+lib))))
                                   (delete-duplicates
                                    (list-sort (lambda (x y)
                                                 (<? compare-names (pkg+lib-pkgname x)
                                                     (pkg+lib-pkgname y)))
                                               pkg+lib*)
                                    (lambda (x y)
                                      (equal? (pkg+lib-pkgname x) (pkg+lib-pkgname y)))))
                              ", "))
                       "\n"))))
            (vector->list libs))))))))

(define (render-manpage filename manpage)
  (putenv "MANPAGE" manpage)
  (let ((manual
         (call/cc
           (lambda (return)
             (let lp ((shtml (html->shtml (open-process "groff -man -Thtml \"$MANPAGE\""))))
               (match shtml
                 [('body . x*) (return x*)]
                 [(tag . x*) (for-each lp x*)]
                 [_ #f]))))))
    (write-shtml filename
                 (template "Akku.scm - manual page"
                           #f
                           manual))))

(define timestamps (call-with-input-file "timestamps.scm" read))

(define keyring (call-with-port (open-file-input-port "Akku-keyring.pgp")
                  get-openpgp-keyring))

(define signature-issuers
  (let ((ht (make-hashtable equal-hash equal?)))
    (for-each (match-lambda
               [(date name version issuers)
                (hashtable-set! ht (cons name version) issuers)])
              timestamps)
    ht))

(define metadata                        ;(name . version) => pkgmeta
  (call-with-input-file metadata-file
    (lambda (p)
      (let ((ht (make-hashtable equal-hash equal?)))
        (let lp ()
          (match (read p)
            [(? eof-object?) #t]
            [('package-version ('name name)
                               ('version version)
                               ('lock lock)
                               ('license license)
                               ('artifacts . artifacts)
                               . _)
             (hashtable-set! ht (cons name version)
                             (make-pkgmeta name version lock license artifacts))
             (lp)]
            [else (lp)]))
        ht))))

(write-shtml "build/index.html" (make-site-index timestamps))

(mkdir/recursive "build/privacy")
(write-shtml "build/privacy/index.html" (make-privacy-page))

(mkdir/recursive "build/packages")

(let ((index (read-package-index "Akku-origin.scm"
                                 #;(path-join (application-data-directory) "index.db"))))
  (write-shtml "build/packages/index.html"
               (make-package-index index))
  (render-packages index timestamps keyring signature-issuers metadata)

  (let-values ([(libraries identifiers) (gather-libraries+identifiers index metadata)])
    (mkdir/recursive "build/identifiers")
    (let ((pages (make-identifier-index-pages identifiers)))
      (write-shtml "build/identifiers/index.html" (car pages))
      (for-each (lambda (page n)
                  (write-shtml (fmt #f "build/identifiers/" n ".html")
                               page))
                pages
                (iota (length pages) 1)))

    (mkdir/recursive "build/libraries")
    (write-shtml "build/libraries/index.html" (make-library-index libraries))))

#;
(write-shtml "build/libraries/index.html"
             (make-library-index metadata))

(mkdir/recursive "build/docs")
(render-manpage "build/docs/manpage.html"
                ".akku/src/akku/docs/akku.1") ;a bit presuming
