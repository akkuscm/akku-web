#!/bin/sh

set -e
rm -rf builg

. .akku/bin/activate
set -x
curl -fO https://archive.akkuscm.org/archive/Akku-origin.scm.xz
xz -df Akku-origin.scm.xz
curl -fO https://archive.akkuscm.org/archive/timestamps.scm
curl -fO https://archive.akkuscm.org/archive/Akku-keyring.pgp
curl https://archive.akkuscm.org/archive/metadata.scm.xz | xz -d > metadata.scm
mkdir -p build/cgi-bin
cp -a bin/akku-put.sps build/cgi-bin/akku-put
bin/sitegen.sps
/bin/cp -a public/assets/ build/

bin/gophergen.sps

if [ -d /srv/akkuscm.org ]; then
    /bin/cp -a build/* /srv/akkuscm.org
fi

. config

for dest in $SERVERS; do
    rsync -av build/ $dest
done

for dest in $GOPHER_SERVERS; do
    rsync -av --delete-after builg/ $dest
done
